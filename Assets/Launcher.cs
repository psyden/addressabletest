﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class Launcher : MonoBehaviour
{
    private PrefabsConfig _prefabsCfg;
    
    void Start()
    {
        _prefabsCfg = new PrefabsConfig();

//        GameObject prefab = Resources.Load<GameObject>("Prefabs/Enemy1");
//        GameObject prefab2 = Resources.Load<GameObject>("Prefabs/Enemy2");
//        Instantiate(prefab, Vector3.left, Quaternion.identity);
//        Instantiate(prefab2, Vector3.right, Quaternion.identity);
        
        StartCoroutine(LoadPrefabs());
    }

    private IEnumerator LoadPrefabs()
    {
        
        List<string> keys = new List<string>(_prefabsCfg.Prefabs.Keys);
        List<AsyncOperationHandle<GameObject>> operations = new List<AsyncOperationHandle<GameObject>>(keys.Count);

        for (int i = 0; i < keys.Count; i++)
        {
            string assetName = keys[i];
            AsyncOperationHandle<GameObject> op = Addressables.LoadAssetAsync<GameObject>(assetName);
            operations.Add(op);
        }

        int loadedQuantity;
        do
        {
            yield return null;
            loadedQuantity = 0;
            for (int i = 0; i < keys.Count; i++)
            {
                string key = keys[i];
                var operation = operations[i];
                if (operation.Status == AsyncOperationStatus.Succeeded)
                {
                    _prefabsCfg.Prefabs[key] = operation.Result;
                    loadedQuantity += 1;
                }
                else if (operation.Status == AsyncOperationStatus.Failed)
                {
                    loadedQuantity += 1;
                    Debug.LogError($"Asset loading failed: {key}");
                }
            }
            Debug.Log("roman");
        } while (loadedQuantity < keys.Count);

        GameObject.Instantiate(_prefabsCfg.Prefabs[PrefabsConfig.Enemy1], Vector3.left, Quaternion.identity);
        GameObject.Instantiate(_prefabsCfg.Prefabs[PrefabsConfig.Enemy2], Vector3.right, Quaternion.identity);

    }

}

public class PrefabsConfig
{
    
    public static string Enemy1 = "Enemy1";
    public static string Enemy2 = "Enemy2";

    public Dictionary<string, GameObject> Prefabs;

    public PrefabsConfig()
    {
        Prefabs = new Dictionary<string, GameObject>();
        Prefabs.Add(Enemy1, null);
        Prefabs.Add(Enemy2, null);
    }
}
